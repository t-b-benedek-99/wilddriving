﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadSceneButton : MonoBehaviour
{
    [SerializeField]
    private string sceneToLoad;

    public GameObject loader;
    
    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(LoadScene);
    }

    public void LoadScene()
    {
        gameObject.SetActive(false);
        loader.SetActive(true);
        loader.GetComponent<ProgressSceneLoader>().LoadScene(sceneToLoad);
    }
}
