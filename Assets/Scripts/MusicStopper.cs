﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicStopper : MonoBehaviour
{

    public GameObject[] musics;

    void Start()
    {
        musics = GameObject.FindGameObjectsWithTag("Music");

        foreach (GameObject music in musics) {
            //music.FindGameObjectWithTag("Music").GetComponent<MusicClass>().StopMusic();
            music.GetComponent<MusicClass>().StopMusic();
        }
    }
}
