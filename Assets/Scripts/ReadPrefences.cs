﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class ReadPrefences : MonoBehaviour
{

    public GameObject mySpeedo;
    public GameObject normalMiniMap;
    public GameObject bigMiniMap;
    public GameObject goBackMusic;
    public GameObject levelMusic;

    void Start()
    {
        GetSpeedometerStatus();
        GetMiniMapStatus();
        GetMenuMusicStatus();
        GetGameMusicStatus();
    }

    public void GetSpeedometerStatus()
    {
        switch (PlayerPrefs.GetInt("SpeedometerEnabled"))
        {
            case 0:
                mySpeedo.SetActive(false);
                break;
            case 1:
                mySpeedo.SetActive(true);
                break;
        }
    }

    public void GetMiniMapStatus()
    {
        switch (PlayerPrefs.GetInt("MiniMapEnabled"))
        {
            case 0:
                normalMiniMap.SetActive(false);
                bigMiniMap.SetActive(false);
                break;
            case 1:
                switch (PlayerPrefs.GetInt("MiniMapSize"))
                {
                    case 0:
                        normalMiniMap.SetActive(true);
                        break;
                    case 1:
                        bigMiniMap.SetActive(true);
                        break;
                }                
                break;
        }
    }

    public void GetMenuMusicStatus()
    {
        switch (PlayerPrefs.GetInt("MenuMusicEnabled"))
        { 
            case 0:
                goBackMusic.SetActive(false);
                break;
            case 1:
                goBackMusic.SetActive(true);
                break;
        }
    }

    public void GetGameMusicStatus()
    {
        switch (PlayerPrefs.GetInt("SessionMusicEnabled"))
        {
            case 0:
                levelMusic.SetActive(false);
                break;
            case 1:
                levelMusic.SetActive(true);
                break;
        }
    }

}
