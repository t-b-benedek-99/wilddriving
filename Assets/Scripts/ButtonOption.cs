﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonOption : MonoBehaviour
{

    public GameObject[] musics;


    public void PlayGame()
    {
        SceneManager.LoadScene(3);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void TrackSelect()
    {
        SceneManager.LoadScene(3);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(2);
    }

    //Below here are track selection buttons

    public void Track01()
    {
        SceneManager.LoadScene(4);
    }

    public void Track02()
    {
        SceneManager.LoadScene(5);
    }
    
    public void Track03()
    {
        SceneManager.LoadScene(6);
    }

    public void Track04()
    {
        SceneManager.LoadScene(7);
    }

    public void SettingsMenu()
    {
        SceneManager.LoadScene(8);
    }

    public void SaveSpeedometerStatus(bool isEnabled)
    {
        switch (isEnabled)
        {
            case true:
                PlayerPrefs.SetInt("SpeedometerEnabled", 1);
                break;
            case false:
                PlayerPrefs.SetInt("SpeedometerEnabled", 0);
                break;
        }
    }

    public void SaveMiniMapStatus(bool isEnabled)
    {
        switch (isEnabled)
        {
            case true:
                PlayerPrefs.SetInt("MiniMapEnabled", 1);
                break;
            case false:
                PlayerPrefs.SetInt("MiniMapEnabled", 0);
                break;
        }
    }

    public void SaveMenuMusicStatus(bool isEnabled)
    {
        musics = GameObject.FindGameObjectsWithTag("Music");
        switch (isEnabled)
        {
            case true:
                PlayerPrefs.SetInt("MenuMusicEnabled", 1);
                foreach (GameObject music in musics)
                {
                    //music.FindGameObjectWithTag("Music").GetComponent<MusicClass>().StopMusic();
                    music.GetComponent<MusicClass>().PlayMusic();
                }
                break;
            case false:
                PlayerPrefs.SetInt("MenuMusicEnabled", 0);
                foreach (GameObject music in musics)
                {
                    //music.FindGameObjectWithTag("Music").GetComponent<MusicClass>().StopMusic();
                    music.GetComponent<MusicClass>().StopMusic();
                }
                break;
        }
    }

    public void SaveGameMusicStatus(bool isEnabled)
    {
        switch (isEnabled)
        {
            case true:
                PlayerPrefs.SetInt("SessionMusicEnabled", 1);
                break;
            case false:
                PlayerPrefs.SetInt("SessionMusicEnabled", 0);
                break;
        }
    }

    public void SaveMiniMapSize(int option)
    {
        switch (option)
        {
            case 0:
                PlayerPrefs.SetInt("MiniMapSize", 0); // Normal
                break;
            case 1:
                PlayerPrefs.SetInt("MiniMapSize", 1); // Big
                break;
            case 2:
                PlayerPrefs.SetInt("MiniMapSize", 2); // Small
                break;
        }
    }
}

