﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomScene : MonoBehaviour
{
    private string levelToLoad;

    public GameObject loader;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(LoadLevel);
    }

    public void LoadLevel()
    {
        string[] levelNames = new string[] { "RaceArea01", "RaceArea02", "RaceArea03", "RaceArea04", "RaceArea05", "RaceArea06" };
        System.Random rnd = new System.Random();
        int randomNum = rnd.Next(6);
        levelToLoad = levelNames[randomNum];
        int carDecision = rnd.Next(1,3);
        GlobalCar.CarType = carDecision;
        gameObject.SetActive(false);
        loader.SetActive(true);
        loader.GetComponent<ProgressSceneLoader>().LoadScene(levelToLoad);
    }
}
