﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderEnabling : MonoBehaviour
{

    public GameObject Blue;
    public GameObject Red;

    void Start()
    {
        StartCoroutine(DisableColliders());
    }

    private IEnumerator DisableColliders()
    {
        yield return new WaitForSeconds(3.5f);
        Blue.GetComponent<BoxCollider>().enabled = false;
        Red.GetComponent<BoxCollider>().enabled = false;
    }

}
