﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSelect : MonoBehaviour
{
    public GameObject TrackSelectionA;
    public GameObject TrackSelectionB;

    public void SelectRaceMode()
    {
        TrackSelectionA.SetActive(true);
        TrackSelectionB.SetActive(false);
    }

    public void SelectTimeAttack()
    {
        TrackSelectionA.SetActive(false);
        TrackSelectionB.SetActive(true);
    }

}
