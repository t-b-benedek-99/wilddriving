﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarChoice : MonoBehaviour
{

    public GameObject FirstRedBody;
    public GameObject FirstBlueBody;
    public GameObject SecondRedBody;
    public GameObject SecondBlueBody;
    public int CarImport;

    void Start()
    {
        CarImport = GlobalCar.CarType;
        if(CarImport == 1)
        {
            FirstRedBody.SetActive(true);
            SecondBlueBody.SetActive(true);
        }

        if (CarImport == 2)
        {
            FirstBlueBody.SetActive(true);
            SecondRedBody.SetActive(true);
        }
    }

}
