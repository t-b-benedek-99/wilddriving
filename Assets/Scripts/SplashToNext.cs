﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashToNext : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(ToNextSplash());
    }

    IEnumerator ToNextSplash()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }
}
