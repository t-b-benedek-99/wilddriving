﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class ShowCurrentSpeed : MonoBehaviour
{
    public GameObject myCar;
    public float speed;

    void Update()
    {
        speed = myCar.gameObject.GetComponent<CarController>().CurrentSpeed;
        AnalogueSpeedConverter.ShowSpeed(speed, 0, 280);
    }
}
