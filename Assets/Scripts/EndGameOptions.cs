﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameOptions : MonoBehaviour
{
    public GameObject musicBox;

    public void Replay()
    {
        if (Time.timeScale == 0.0f)
        {
            Time.timeScale = 1.0f;
        }
        StartCoroutine(Reload());
    }

    public void GoToMainMenu()
    {
        musicBox.GetComponent<MusicClass>().PlayMusic();
        SceneManager.LoadScene(2);
    }

    public void Quit()
    {
        Application.Quit();
    }

    private IEnumerator Reload()
    {
        yield return null;
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        Resources.UnloadUnusedAssets();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

}
