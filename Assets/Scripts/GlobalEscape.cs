﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEscape : MonoBehaviour
{

    public GameObject exitPanel;

    public AudioSource LevelMusic;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LevelMusic.Stop();
            exitPanel.SetActive(true);
            if (Time.timeScale == 1.0f)
            {
                Time.timeScale = 0.0f;
            }
            else
            {
                LevelMusic.Play();
                exitPanel.SetActive(false);
                Time.timeScale = 1.0f;
            }
        }
    }
}
