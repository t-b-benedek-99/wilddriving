﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeOptions : MonoBehaviour
{

    public GameObject thisPanel;

    public GameObject musicBox;

    public AudioSource LevelMusic;

    public void ContinueGame()
    {
        thisPanel.SetActive(false);
        LevelMusic.Play();
        Time.timeScale = 1.0f;
    }

    public void GoToMainMenu()
    {
        musicBox.GetComponent<MusicClass>().PlayMusic();
        SceneManager.LoadScene(2);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
