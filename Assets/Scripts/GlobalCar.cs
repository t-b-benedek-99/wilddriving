﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalCar : MonoBehaviour
{
    
    public static int CarType; // 1 : Red; 2: Blue;
    public GameObject ModeWindow;

    public void RedCar()
    {
        CarType = 1;
        ModeWindow.SetActive(true);
    }

    public void BlueCar()
    {
        CarType = 2;
        ModeWindow.SetActive(true);
    }
}
