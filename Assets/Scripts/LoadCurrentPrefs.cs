﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadCurrentPrefs : MonoBehaviour
{

    public GameObject speedoToggler;
    public GameObject miniMapToggler;
    public GameObject menuMusicToggler;
    public GameObject gameMusicToggler;
    public Dropdown dropdown;

    void Start()
    {
        speedoToggler = GameObject.Find("SpeedometerToggle");
        miniMapToggler = GameObject.Find("MiniMapToggle");
        menuMusicToggler = GameObject.Find("MenuMusicToggle");
        gameMusicToggler = GameObject.Find("SessionMusicToggle");
       
        if (PlayerPrefs.GetInt("SpeedometerEnabled") == 0)
        {
            speedoToggler.GetComponent<Toggle>().isOn = false;
        } else if (PlayerPrefs.GetInt("SpeedometerEnabled") == 1)
        {
            speedoToggler.gameObject.GetComponent<Toggle>().isOn = true;
        }

        if (PlayerPrefs.GetInt("MiniMapEnabled") == 0)
        {
            miniMapToggler.GetComponent<Toggle>().isOn = false;
        }
        else if (PlayerPrefs.GetInt("MiniMapEnabled") == 1)
        {
            miniMapToggler.gameObject.GetComponent<Toggle>().isOn = true;
        }

        if (PlayerPrefs.GetInt("MenuMusicEnabled") == 0)
        {
            menuMusicToggler.GetComponent<Toggle>().isOn = false;
        }
        else if (PlayerPrefs.GetInt("MenuMusicEnabled") == 1)
        {
            menuMusicToggler.gameObject.GetComponent<Toggle>().isOn = true;
        }

        if (PlayerPrefs.GetInt("SessionMusicEnabled") == 0)
        {
            gameMusicToggler.GetComponent<Toggle>().isOn = false;
        }
        else if (PlayerPrefs.GetInt("SessionMusicEnabled") == 1)
        {
            gameMusicToggler.gameObject.GetComponent<Toggle>().isOn = true;
        }


        switch (PlayerPrefs.GetInt("MiniMapSize"))
        {
            case 0:
                dropdown.value = 0;
                break;
            case 1:
                dropdown.value = 1;
                break;
            case 2:
                dropdown.value = 2;
                break;
            default:
                dropdown.value = 0;
                break;
        }
    }
}